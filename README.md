# Pooly

Pooly example from Little Elixir & OTP Guidebook revisited.

The pooly example code in the book chapter 6, will give you deprecation warnings when run in Elixir 1.11+. This version tries to fix that with the following changes

1) to use DynamicSupervisor in `Pooly.WorkerSupervisor` instead of `:simple_one_for_one` strategy from Supervisor
2) passing a child_spec type into `pool_config` instead of {m, f, a}.

## Installation

Clone are run

```sh
git clone git@gitlab.com:tristanperalta/pooly.git

cd pooly

iex -S mix

# you may check workers through :observer.start()
```
