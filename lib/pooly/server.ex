defmodule Pooly.Server do
  use GenServer

  defmodule State,
    do:
      defstruct(
        sup: nil,
        worker_sup: nil,
        size: nil,
        workers: nil,
        mfa: nil,
        monitors: nil
      )

  def start_link([sup, pool_config]) do
    GenServer.start_link(__MODULE__, [sup, pool_config], name: __MODULE__)
  end

  def checkout do
    GenServer.call(__MODULE__, :checkout)
  end

  def checkin(worker_pid) do
    GenServer.cast(__MODULE__, {:checkin, worker_pid})
  end

  def status() do
    GenServer.call(__MODULE__, :status)
  end

  @impl true
  def init([sup, pool_config]) when is_pid(sup) do
    Process.flag(:trap_exit, true)

    init(pool_config, %State{
      sup: sup,
      monitors: :ets.new(:monitors, [:private])
    })
  end

  def init([{:mfa, mfa} | rest], state) do
    init(rest, %{state | mfa: mfa})
  end

  def init([{:size, size} | rest], state) do
    init(rest, %{state | size: size})
  end

  def init([_ | rest], state), do: init(rest, state)

  def init([], state) do
    {:ok, state, {:continue, :start_worker_supervisor}}
  end

  @impl true
  def handle_continue(:start_worker_supervisor, state = %{sup: sup, mfa: worker_spec, size: size}) do
    {:ok, worker_sup} =
      Supervisor.start_child(
        sup,
        Supervisor.child_spec({Pooly.WorkerSupervisor, []}, restart: :temporary)
      )

    workers = prepopulate(size, {worker_sup, worker_spec})
    {:noreply, %{state | worker_sup: worker_sup, workers: workers}}
  end

  @impl true
  def handle_info({:DOWN, ref, _, _, _}, state = %{monitors: monitors, workers: workers}) do
    case :ets.match(monitors, {:"$1", ref}) do
      [[pid]] ->
        true = :ets.delete(monitors, pid)
        # returns the worker to the pool
        new_state = %{state | workers: [pid | workers]}
        {:noreply, new_state}

      [[]] ->
        {:noreply, state}
    end
  end

  def handle_info(
        {:EXIT, pid, _reason},
        state = %{monitors: monitors, workers: workers, mfa: worker_spec, worker_sup: worker_sup}
      ) do
    case :ets.lookup(monitors, pid) do
      [{pid, ref}] ->
        true = Process.demonitor(ref)
        true = :ets.delete(monitors, pid)
        new_state = %{state | workers: [new_worker(worker_sup, worker_spec) | workers]}
        {:noreply, new_state}

      [[]] ->
        {:noreply, state}
    end
  end

  @impl true
  def handle_call(:checkout, {from_pid, _ref}, %{workers: workers, monitors: monitors} = state) do
    case workers do
      [worker | rest] ->
        ref = Process.monitor(from_pid)
        true = :ets.insert(monitors, {worker, ref})
        {:reply, worker, %{state | workers: rest}}

      [] ->
        {:reply, :noproc, state}
    end
  end

  def handle_call(:status, _from, %{workers: workers, monitors: monitors} = state) do
    {:reply, {length(workers), :ets.info(monitors, :size)}, state}
  end

  def handle_call({:checkin, worker}, %{workers: workers, monitors: monitors} = state) do
    case :ets.lookup(monitors, worker) do
      [{pid, ref}] ->
        true = Process.demonitor(ref)
        true = :ets.delete(monitors, pid)
        {:noreply, %{state | workers: [pid | workers]}}

      [] ->
        {:noreply, state}
    end
  end

  defp prepopulate(size, pair), do: prepopulate(size, pair, [])

  defp prepopulate(0, _sup, workers), do: workers

  defp prepopulate(size, {sup, worker_spec} = pair, workers) do
    prepopulate(size - 1, pair, [new_worker(sup, worker_spec) | workers])
  end

  defp new_worker(worker_sup, worker_spec) do
    {:ok, worker} = Pooly.WorkerSupervisor.start_work(worker_sup, worker_spec)
    worker
  end
end
