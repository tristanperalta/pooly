defmodule Pooly.WorkerSupervisor do
  use DynamicSupervisor

  def start_link(_) do
    DynamicSupervisor.start_link(__MODULE__, nil, name: __MODULE__)
  end

  def init(_) do
    opts = [strategy: :one_for_one, max_restarts: 5, max_seconds: 5]
    DynamicSupervisor.init(opts)
  end

  def start_work(worker_sup, worker_spec) do
    DynamicSupervisor.start_child(worker_sup, worker_spec)
  end
end
